import random

numeros_aleatorios = []

for i in range(10):
  numeros_aleatorios.append(random.randint(1,100))

pares = []
impares = []

for i in range(len(numeros_aleatorios)):
  if (numeros_aleatorios[i]%2)==0:
    pares.append(numeros_aleatorios[i])
  else:
    impares.append(numeros_aleatorios[i])

print(f'Essa foi a lista gerada aleatoriamente:\n{numeros_aleatorios}\n\nPares: {pares}\nImpares: {impares}')