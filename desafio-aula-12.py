respostas = []
perguntas = ['Telefonou para a vítima?', 'Esteve no local do crime?', 'Mora perto da vítima?', 'Devia para a vítima?',
             'Já trabalhou com a vítima?']

print('Responda apenas com s (caso sim) ou n (caso não)\n')

i=0
while i<5:
    resp = input(f'{perguntas[i]}\n')
    if resp!='n' and resp!='s':
        print('Digite um valor válido! Apesar "s" ou "n"')
    else:
        respostas.append(resp)
        i+=1

if respostas.count('s')==5:
    print('Você foi classificado como assassino!')
elif respostas.count('s')>2:
    print('Você foi classificado como cúmplice!')
elif respostas.count('s')==2:
    print('Você foi classificado como suspeito...')
else:
    print('Você foi classificado como inocente :)')