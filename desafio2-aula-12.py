frase = input('Digite uma frase: \n')
letra_desejada = input('Digite a letra que quer contar: \n')

cont = 0

for letra in frase:
  if letra==letra_desejada:
    cont += 1

print(f'A letra {letra_desejada} aparece {cont} vezes na frase:\n"{frase}"')